package org.accesshub.server.services;

import org.accesshub.server.commands.CreateUserProfileCommand;
import org.accesshub.server.commands.EditUserProfileCommand;
import org.accesshub.server.dto.UserProfileCreateDTO;
import org.accesshub.server.dto.UserProfileUpdateDTO;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class UserProfileCommandService {

	private final CommandGateway commandGateway;

	public UserProfileCommandService(CommandGateway commandGateway) {
		this.commandGateway = commandGateway;
	}


	public CompletableFuture<String> createUserProfile(UserProfileCreateDTO userProfileCreateDTO) {
		return commandGateway.send(new CreateUserProfileCommand(userProfileCreateDTO.getUsername(), userProfileCreateDTO.getFirstName(), userProfileCreateDTO.getLastName(), userProfileCreateDTO.getEmail()));
	}

	public CompletableFuture<String> updateUserProfile(String username, UserProfileUpdateDTO userProfileUpdateDTO) {
		EditUserProfileCommand editUserProfileCommand = new EditUserProfileCommand(username, userProfileUpdateDTO.getFirstName(), userProfileUpdateDTO.getLastName(), userProfileUpdateDTO.getEmail());
		return commandGateway.send(editUserProfileCommand);
	}
}
