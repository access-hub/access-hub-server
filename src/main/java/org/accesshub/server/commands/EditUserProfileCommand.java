package org.accesshub.server.commands;

import lombok.Getter;

@Getter
public class EditUserProfileCommand extends BaseCommand<String> {


	private final String firstName;
	private final String lastName;
	private final String email;

	public EditUserProfileCommand(String id, String firstName, String lastName, String email) {
		super(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
}
