package org.accesshub.server.controllers;

import io.swagger.annotations.Api;
import org.accesshub.server.dto.UserProfileCreateDTO;
import org.accesshub.server.dto.UserProfileUpdateDTO;
import org.accesshub.server.models.UserProfile;
import org.accesshub.server.querys.FindUserProfileQuery;
import org.accesshub.server.services.UserProfileCommandService;
import org.accesshub.server.services.UserProfileQueryService;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/user-profiles")
@Api(value = "User Profiles", tags = {"User"})
public class UserProfileController {

	private final UserProfileCommandService userProfileCommandService;
	private final UserProfileQueryService userProfileQueryService;
	private final QueryGateway queryGateway;


	public UserProfileController(UserProfileCommandService userProfileCommandService, UserProfileQueryService userProfileQueryService, QueryGateway queryGateway) {
		this.userProfileCommandService = userProfileCommandService;
		this.userProfileQueryService = userProfileQueryService;
		this.queryGateway = queryGateway;
	}

	@PostMapping
	public CompletableFuture<String> createUserProfile(@RequestBody UserProfileCreateDTO userProfileCreateDTO) {
		return userProfileCommandService.createUserProfile(userProfileCreateDTO);
	}

	@PutMapping(value = "/{username}")
	public CompletableFuture<String> updateUserProfile(@PathVariable(value = "username") String username, @RequestBody UserProfileUpdateDTO userProfileUpdateDTO) {
		return userProfileCommandService.updateUserProfile(username, userProfileUpdateDTO);
	}

	@GetMapping(value = "/{username}")
	public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
		return queryGateway.query(new FindUserProfileQuery(username), ResponseTypes.instanceOf(UserProfile.class)).join();

	}

	@GetMapping(value = "/{username}/events")
	public List<Object> eventHistoryForUserProfile(@PathVariable(value = "username") String username) {
		return userProfileQueryService.getEvents(username);
	}


}
