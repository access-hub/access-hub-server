package org.accesshub.server.events;

import lombok.Getter;

@Getter
public class UserProfileCreatedEvent extends BaseEvent<String> {

	private final String firstName;
	private final String lastName;
	private final String email;

	public UserProfileCreatedEvent(String id, String firstName, String lastName, String email) {
		super(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
}
