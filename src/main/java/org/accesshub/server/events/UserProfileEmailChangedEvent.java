package org.accesshub.server.events;

import lombok.Getter;

@Getter
public class UserProfileEmailChangedEvent extends BaseEvent<String> {


	private final String email;

	public UserProfileEmailChangedEvent(String id, String email) {
		super(id);
		this.email = email;
	}
}
