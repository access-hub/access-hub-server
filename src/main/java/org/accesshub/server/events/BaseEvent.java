package org.accesshub.server.events;

public abstract class BaseEvent<T> {

	public final T id;

	public BaseEvent(T id) {
		this.id = id;
	}
}
