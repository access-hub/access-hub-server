package org.accesshub.server.events;

import lombok.Getter;
import org.accesshub.server.events.enums.ProfileStatus;

@Getter
public class UserProfileActivatedEvent extends BaseEvent<String> {


	private final ProfileStatus status;

	public UserProfileActivatedEvent(String id, ProfileStatus status) {
		super(id);
		this.status = status;
	}
}
